package company.fridge.fetcher;


import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.Map;

import company.fridge.activity.AuthorizedAppCompatActivity;
import company.fridge.model.UserDataGetModel;
import company.fridge.manager.UserPreferencesManager;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.EntityUtils;

public class UserDataResponseFetcher extends AsyncTask<Object, Object, UserDataGetModel> {

    private AuthorizedAppCompatActivity activity;
    private Map<String, String> headers;
    private String uri;

    public UserDataResponseFetcher(AuthorizedAppCompatActivity activity, String uri, Map<String, String> headers) {
        this.activity = activity;
        this.headers = headers;
        this.uri = uri;
    }

    @Override
    protected UserDataGetModel doInBackground(Object... params) {
        return getUserData(uri, headers);
    }

    @Override
    protected void onPostExecute(UserDataGetModel userDataModel) {
        super.onPostExecute(userDataModel);
        String username = "";
        String photoPath = "";
        if (userDataModel != null) {
            if (userDataModel.getImageUrl() != null) {
               photoPath = userDataModel.getImageUrl();
            } if (userDataModel.getUsername() != null) {
                username = userDataModel.getUsername();
            }
        }
        UserPreferencesManager.setProfilePhotoPath(activity, photoPath);
        UserPreferencesManager.setUsername(activity, username);
    }

    private UserDataGetModel getUserData(String uri, Map<String, String> headers) {
        int requestCode;
        UserDataGetModel userDataGetModel = null;
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(uri);
        try {
            for (String header : headers.keySet()) {
                httpGet.setHeader(header, headers.get(header));
            }
            HttpResponse httpResponse = httpclient.execute(httpGet);
            String responseContent = EntityUtils.toString(httpResponse.getEntity());
            requestCode = httpResponse.getStatusLine().getStatusCode();
            Log.d(getClass().getSimpleName(), (String) responseContent);
            if (requestCode == 200) {
                userDataGetModel = new Gson().fromJson(responseContent, UserDataGetModel.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userDataGetModel;
    }
}

