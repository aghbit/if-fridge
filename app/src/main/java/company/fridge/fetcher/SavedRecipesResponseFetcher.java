package company.fridge.fetcher;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.Map;

import company.fridge.activity.AuthorizedAppCompatActivity;
import company.fridge.model.SavedRecipesGetModel;
import company.fridge.db.RecipesDataSource;
import company.fridge.manager.SavedRecipesManager;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.EntityUtils;

public class SavedRecipesResponseFetcher extends AsyncTask<Object, Object, SavedRecipesGetModel> {

    private AuthorizedAppCompatActivity authorizedAppCompatActivity;
    private RecipesDataSource recipesDataSource;
    private Map<String, String> headers;
    private String uri;


    public SavedRecipesResponseFetcher(AuthorizedAppCompatActivity activity, RecipesDataSource recipesDataSource, String uri,
                                       Map<String, String> headers) {
        this.authorizedAppCompatActivity = activity;
        this.recipesDataSource = recipesDataSource;
        this.headers = headers;
        this.uri = uri;
    }

    @Override
    protected SavedRecipesGetModel doInBackground(Object... params) {
        return getSavedRecipes(uri, headers);
    }

    @Override
    protected void onPostExecute(SavedRecipesGetModel savedRecipesGetModel) {
        super.onPostExecute(savedRecipesGetModel);
        if (savedRecipesGetModel != null) {
            SavedRecipesManager.updateSavedRecipesToThoseReceivedFromServer(savedRecipesGetModel.getTimestamp(),
                    savedRecipesGetModel.getRecipes(), recipesDataSource, authorizedAppCompatActivity, authorizedAppCompatActivity.getProfileId());
        }
    }

    private SavedRecipesGetModel getSavedRecipes(String uri, Map<String, String> headers) {
        int requestCode;
        SavedRecipesGetModel savedRecipesGetModel = null;
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(uri);
        try {
            for (String header : headers.keySet()) {
                httpGet.setHeader(header, headers.get(header));
                Log.d(getClass().getSimpleName(), "H: " + header + " --- " + headers.get(header));
            }
            HttpResponse httpResponse = httpclient.execute(httpGet);
            String responseContent = EntityUtils.toString(httpResponse.getEntity());
            requestCode = httpResponse.getStatusLine().getStatusCode();
            Log.d(getClass().getSimpleName(), (String) responseContent);
            if (requestCode == 200) {
                savedRecipesGetModel = new Gson().fromJson(responseContent, SavedRecipesGetModel.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return savedRecipesGetModel;
    }
}

