package company.fridge.fetcher;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.Map;

import company.fridge.activity.AuthorizedAppCompatActivity;
import company.fridge.model.UserPreferencesGetModel;
import company.fridge.manager.UserPreferencesManager;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.EntityUtils;

public class UserPreferencesResponseFetcher extends AsyncTask<Object, Object, UserPreferencesGetModel> {

    private AuthorizedAppCompatActivity activity;
    private Map<String, String> headers;
    private String uri;

    public UserPreferencesResponseFetcher(AuthorizedAppCompatActivity activity, String uri, Map<String, String> headers) {
        this.activity = activity;
        this.headers = headers;
        this.uri = uri;
    }

    @Override
    protected UserPreferencesGetModel doInBackground(Object... params) {
        return getUserLabels(uri, headers);
    }

    @Override
    protected void onPostExecute(UserPreferencesGetModel userPreferencesModel) {
        super.onPostExecute(userPreferencesModel);
        if (userPreferencesModel != null) {
            UserPreferencesManager.updateUserPreferencesFromServer(activity, userPreferencesModel.getHealthPrefs(), userPreferencesModel.getDietPrefs(), userPreferencesModel.getTimestamp());
        }
    }

    private UserPreferencesGetModel getUserLabels(String uri, Map<String, String> headers) {
        int requestCode;
        UserPreferencesGetModel userPreferencesModel = null;
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(uri);
        try {
            for (String header : headers.keySet()) {
                httpGet.setHeader(header, headers.get(header));
                Log.d(getClass().getSimpleName(), "H: " + header + " --- " + headers.get(header));
            }
            HttpResponse httpResponse = httpclient.execute(httpGet);
            String responseContent = EntityUtils.toString(httpResponse.getEntity());
            requestCode = httpResponse.getStatusLine().getStatusCode();
            Log.d(getClass().getSimpleName(), (String) responseContent);
            if (requestCode == 200) {
                userPreferencesModel = new Gson().fromJson(responseContent, UserPreferencesGetModel.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userPreferencesModel;
    }
}

