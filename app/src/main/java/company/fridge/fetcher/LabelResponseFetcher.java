package company.fridge.fetcher;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import company.fridge.activity.AuthorizedAppCompatActivity;
import company.fridge.model.LabelGetModel;
import company.fridge.request.UrlCreator;
import company.fridge.service.DataService;
import company.fridge.service.DataServiceImpl;
import company.fridge.manager.UserPreferencesManager;

public class LabelResponseFetcher extends AsyncTask<Object, Object, Void> {

    private AuthorizedAppCompatActivity activity;
    private LabelGetModel labelResponse;

    public LabelResponseFetcher(AuthorizedAppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    protected Void doInBackground(Object... params) {
        getLabels();
        return null;
    }

    @Override
    protected void onPostExecute(Void object) {
        super.onPostExecute(object);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                UserPreferencesManager.updatePreferencesList(activity, labelResponse.getHealthLabels(), labelResponse.getDietLabels());
                UserPreferencesManager.getUserPreferencesFromServer(activity);
            }
        });
    }

    private void getLabels() {
        final UrlCreator urlCreator = new UrlCreator();
        final String url = urlCreator.createForLabels();

        final DataService dataService = new DataServiceImpl();
        final String content = dataService.getContentFromUrl(url, "");

        labelResponse = new Gson().fromJson(content, LabelGetModel.class);
        Log.d(getClass().getSimpleName(), "labels response from server: " + labelResponse);
        if (labelResponse == null) {
            labelResponse = new Gson().fromJson(
                    "{ \"healthPrefs\" : [ \"alcohol-free\", \"celery-free\", \"crustacean-free\", \"dairy-free\", \"egg-free\" ]," +
                            " \"dietPrefs\" : [ \"balanced\", \"low-sodium\" ] }",
                    LabelGetModel.class);
        }

    }
}
