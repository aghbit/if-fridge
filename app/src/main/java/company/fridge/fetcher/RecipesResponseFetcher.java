package company.fridge.fetcher;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import company.fridge.activity.RecipeSwipeActivity;
import company.fridge.model.RecipesResponseGetModel;
import company.fridge.request.UrlCreator;
import company.fridge.service.DataService;
import company.fridge.service.DataServiceImpl;

public class RecipesResponseFetcher extends AsyncTask<Object, Object, Void> {

    private final RecipeSwipeActivity swipeActivity;
    private RecipesResponseGetModel model;
    private String authorizationToken;

    public RecipesResponseFetcher(RecipeSwipeActivity swipeActivity, String authorizationToken) {
        this.swipeActivity = swipeActivity;
        this.authorizationToken = authorizationToken;
    }

    @Override
    protected Void doInBackground(Object... params) {
        getRecipes();
        return null;
    }

    @Override
    protected void onPostExecute(Void object) {
        super.onPostExecute(object);
        swipeActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (model != null && !model.getRecipes().isEmpty()) {
                    swipeActivity.setRecipes(model.getRecipes());
                } else {
                    Toast.makeText(swipeActivity, "The list of matching recipes is empty :(", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getRecipes() {
        final UrlCreator urlCreator = new UrlCreator();
        final String url = urlCreator.createForRecipes(swipeActivity.getUrlData());

        final DataService dataService = new DataServiceImpl();
        final String content = dataService.getContentFromUrl(url, authorizationToken);

        if (content != null) {
            model = new Gson().fromJson(content, RecipesResponseGetModel.class);
            Log.d(getClass().getSimpleName(), "model: " + model);
        }
    }
}
