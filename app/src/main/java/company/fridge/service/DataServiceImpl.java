package company.fridge.service;

import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import company.fridge.model.DataPostModel;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.BasicResponseHandler;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


public class DataServiceImpl implements DataService {

    @Override
    public String getContentFromUrl(String uri, String token) {
        HttpClient Client = HttpClientBuilder.create().build();
        String SetServerString;
        try {
            Log.d(getClass().getSimpleName(), "Trying to get " + uri + " from server");
            HttpGet httpGet = new HttpGet(uri);
            httpGet.setHeader("Authorization", token);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            SetServerString = Client.execute(httpGet, responseHandler);
        } catch (Exception ex) {
            Log.d(getClass().getSimpleName(), "GET recipes error");
            return null;
        }
        return SetServerString;
    }

    @Override
    public void postContentToUrl(String uri, DataPostModel model) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(uri);
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("data", String.valueOf(model.getNumber())));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            client.execute(httpPost);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
