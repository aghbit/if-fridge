package company.fridge.service;

import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
import cz.msebera.android.httpclient.entity.mime.content.FileBody;
import cz.msebera.android.httpclient.entity.mime.content.StringBody;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.EntityUtils;


public class AsyncJsonTaskForUserData extends AsyncTask<Object, Void, Object> {

    private int requestCode = 0;
    private AsyncCaller caller;
    private String username;
    private Map<String, String> headers;
    private String uri;
    private File file;

    public AsyncJsonTaskForUserData(AsyncCaller caller, int requestCode, String uri, String username, File file,
                                     Map<String, String> headers) {
        this.caller = caller;
        this.requestCode = requestCode;
        this.username = username;
        this.headers = headers;
        this.uri = uri;
        this.file = file;
    }

    @Override
    protected Object doInBackground(Object... params) {
        HttpClient httpclient = HttpClientBuilder.create().build();
        String responseContent = "";
        try {
            String boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW";

            HttpPost httpPost = new HttpPost(uri);
            MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
            entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            ContentType contentType = ContentType.MULTIPART_FORM_DATA;
            entityBuilder.setBoundary(boundary);
            entityBuilder.addPart("image", new FileBody(file, contentType, file.getName()));
            entityBuilder.addPart("nickname", new StringBody(username, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = entityBuilder.build();

            for (String header : headers.keySet()) {
                httpPost.setHeader(header, headers.get(header));
            }
            httpPost.setEntity(entity);

            Log.d(AsyncJsonTaskForUserData.class.getSimpleName(), httpPost.toString());
            Log.d(AsyncJsonTaskForUserData.class.getSimpleName(), "nickname: " + username + ", image: " + file.getName());
            HttpResponse httpResponse = httpclient.execute(httpPost);
            responseContent = EntityUtils.toString(httpResponse.getEntity());
            requestCode = httpResponse.getStatusLine().getStatusCode();
            Log.d(getClass().getSimpleName(), responseContent);
            return responseContent;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseContent;
    }

    @Override
    protected void onPostExecute(Object result) {
        caller.onBackgroundTaskCompleted(requestCode, result);
    }
}