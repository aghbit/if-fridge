package company.fridge.service;


public interface AsyncCaller {
    void onBackgroundTaskCompleted(int requestCode, Object result);
}
