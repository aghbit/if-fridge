package company.fridge.service;

import company.fridge.model.DataPostModel;

public interface DataService {

    String getContentFromUrl(String uri, String token);
    void postContentToUrl(String uri, DataPostModel model);
}
