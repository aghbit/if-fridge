package company.fridge.service;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import company.fridge.request.HttpDeleteWithBody;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpEntityEnclosingRequestBase;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.EntityUtils;


public class AsyncJsonTask extends AsyncTask<Object, Void, Object> {

    private int requestCode = 0;
    private AsyncCaller caller;
    private JSONObject JSONObjectData;
    private Map<String, String> headers;
    private HttpEntityEnclosingRequestBase base;

    private AsyncJsonTask(AsyncCaller caller, int requestCode, JSONObject JSONObjectData, Map<String, String> headers, HttpEntityEnclosingRequestBase base) {
        this.caller = caller;
        this.requestCode = requestCode;
        this.JSONObjectData = JSONObjectData;
        this.headers = headers;
        this.base = base;
    }

    public static AsyncJsonTask getPostTask(AsyncCaller caller, int requestCode, String uri, JSONObject JSONObjectData, Map<String, String> headers) {
        return new AsyncJsonTask(caller, requestCode, JSONObjectData, headers, new HttpPost(uri));
    }

    public static AsyncJsonTask getDeleteTask(AsyncCaller caller, int requestCode, String uri, JSONObject JSONObjectData, Map<String, String> headers) {
        return new AsyncJsonTask(caller, requestCode, JSONObjectData, headers, new HttpDeleteWithBody(uri));
    }

    @Override
    protected Object doInBackground(Object... params) {
        HttpClient httpclient = HttpClientBuilder.create().build();
        String responseContent = "";
        try {
            for (String header : headers.keySet()) {
                base.setHeader(header, headers.get(header));
            }
            StringEntity param = new StringEntity(JSONObjectData.toString());
            base.setEntity(param);
            Log.d(AsyncJsonTask.class.getSimpleName(), base.toString());
            Log.d(AsyncJsonTask.class.getSimpleName(), JSONObjectData.toString());
            HttpResponse httpResponse = httpclient.execute(base);
            responseContent = EntityUtils.toString(httpResponse.getEntity());
            requestCode = httpResponse.getStatusLine().getStatusCode();
            Log.d(getClass().getSimpleName(), responseContent);
            return responseContent;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseContent;
    }

    @Override
    protected void onPostExecute(Object result) {
        caller.onBackgroundTaskCompleted(requestCode, result);
    }
}