package company.fridge.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import company.fridge.activity.AuthorizedAppCompatActivity;
import company.fridge.activity.MainActivity;
import company.fridge.activity.RecipeSwipeActivity;
import company.fridge.adapter.IngredientsAdapter;
import company.fridge.model.Ingredient;
import company.fridge.manager.InternetConnectionManager;
import company.fridge.R;

public class IngredientsFragment extends Fragment {

    private static final String ARG_INGREDIENTS = "ingredients";
    public static final String ARG_SELECTED_INGREDIENTS = "selected ingredients";

    private ArrayList<Ingredient> ingredients;
    private ArrayList<String> selectedIngredients;
    private MainActivity mainActivity;

    public IngredientsFragment() {
    } // public empty constructor required

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mainActivity = (MainActivity) getActivity();

        if (savedInstanceState != null) {
            ingredients = savedInstanceState.getParcelableArrayList(ARG_INGREDIENTS);
        } else {
            ingredients = new ArrayList<>();
            for (String ingredientName : getResources().getStringArray(R.array.ingredients_names)) {
                ingredients.add(new Ingredient(ingredientName));
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_ingredients, container, false);
        final EditText editText = (EditText) view.findViewById(R.id.filter);
        final ListView listView = (ListView) view.findViewById(R.id.ingredients_list_view);
        final IngredientsAdapter adapter = new IngredientsAdapter(getContext(), ingredients);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        listView.setAdapter(adapter);

        final FloatingActionButton startButton = (FloatingActionButton) view.findViewById(R.id.swipe);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), RecipeSwipeActivity.class);
                getCheckedItems(ingredients, selectedIngredients);
                if (!selectedIngredients.isEmpty() && InternetConnectionManager.isNetworkConnected(mainActivity)) {
                    intent.putStringArrayListExtra(ARG_SELECTED_INGREDIENTS, selectedIngredients);
                    intent.putExtra(AuthorizedAppCompatActivity.ARG_AUTHORIZATION_TOKEN, mainActivity.getAuthorizationToken());
                    intent.putExtra(AuthorizedAppCompatActivity.ARG_PROFILE_ID, mainActivity.getAuthorizationToken());
                    intent.putExtra(AuthorizedAppCompatActivity.ARG_FACEBOOK_ID, mainActivity.getFacebookId());
                    startActivity(intent);
                }
            }
        });
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(ARG_INGREDIENTS, ingredients);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        selectedIngredients = new ArrayList<>();
    }

    private void getCheckedItems(List<Ingredient> ingredients, ArrayList<String> selectedIngredients) {
        for (Ingredient ingredient : ingredients) {
            if (ingredient.getCheckbox()) {
                selectedIngredients.add(ingredient.getName());
            }
        }
    }
}
