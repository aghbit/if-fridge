package company.fridge.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import company.fridge.activity.MainActivity;
import company.fridge.adapter.SavedRecipesAdapter;
import company.fridge.db.RecipeDO;
import company.fridge.db.RecipesDataSource;
import company.fridge.R;

public class SavedRecipesFragment extends Fragment {

    private RecipesDataSource recipesDataSource;
    private SavedRecipesAdapter adapter;
    private List<RecipeDO> savedRecipes;

    public SavedRecipesFragment() {
    }  // public empty constructor required

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.recipesDataSource = ((MainActivity) getActivity()).getRecipesDataSource();
        recipesDataSource.startListening(this);
        savedRecipes = getRecipesFromDatabase();
        Log.d(getClass().getSimpleName(), "Saved Recipes: " + savedRecipes);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (recipesDataSource == null) {
            recipesDataSource = ((MainActivity) getActivity()).getRecipesDataSource();
        }
        recipesDataSource.startListening(this);

        savedRecipes = getRecipesFromDatabase();
        adapter.setRecipeList(savedRecipes);
        adapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_saved_recipes, container, false);
        final ListView listView = (ListView) view.findViewById(R.id.saved_recipes_list_view);

        this.adapter = new SavedRecipesAdapter(this, recipesDataSource, R.layout.saved_recipe, savedRecipes);
        listView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (recipesDataSource != null) {
            recipesDataSource.stopListening();
        }
    }

    public void notifyRecipesListChanged() {
        savedRecipes = getRecipesFromDatabase();
        adapter.setRecipeList(savedRecipes);
        adapter.notifyDataSetChanged();
        Log.d(getClass().getSimpleName(), "Recipes reloaded" + String.valueOf(savedRecipes.size()));
    }

    private List<RecipeDO> getRecipesFromDatabase() {
        return recipesDataSource.getAllRecipes();
    }
}
