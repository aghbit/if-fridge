package company.fridge.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import company.fridge.activity.AuthorizedAppCompatActivity;
import company.fridge.activity.MainActivity;
import company.fridge.activity.WelcomeActivity;
import company.fridge.adapter.PreferencesAdapter;
import company.fridge.model.AuthorizationModel;
import company.fridge.model.FacebookUserModel;
import company.fridge.model.Preference;
import company.fridge.manager.UserPreferencesManager;
import company.fridge.R;

import static android.app.Activity.RESULT_OK;

public class UserProfileFragment extends Fragment {

    private static final String ARG_HEALTH_PREFERENCES_LIST = "healthPreferences";
    private static final String ARG_DIET_PREFERENCES_LIST = "dietPreferences";
    private static final int RESULT_LOAD_IMAGE = 1;

    private List<Preference> healthPreferences;
    private List<Preference> dietPreferences;
    private List<Preference> allPreferences;

    private PreferencesAdapter adapter;
    public MainActivity mainActivity;
    private AuthorizationModel authorizationModel;
    private ImageButton imageButton;
    private EditText username;
    private boolean isScheduledRefresh = false;


    public UserProfileFragment() {
    } // public empty constructor required

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.mainActivity = (MainActivity) getActivity();
        this.authorizationModel
                = new AuthorizationModel(mainActivity.getProfileId(), mainActivity.getAuthorizationToken(), mainActivity.getFacebookId());

        if (savedInstanceState != null) {
            healthPreferences = savedInstanceState.getParcelableArrayList(ARG_HEALTH_PREFERENCES_LIST);
            dietPreferences = savedInstanceState.getParcelableArrayList(ARG_DIET_PREFERENCES_LIST);
        } else {
            healthPreferences = UserPreferencesManager.getHealthPreferences(getActivity());
            dietPreferences = UserPreferencesManager.getDietPreferences(getActivity());
        }

        allPreferences = new ArrayList<>();
        allPreferences.addAll(healthPreferences);
        allPreferences.addAll(dietPreferences);
    }

    public void refreshUserData() {
        username.setText(UserPreferencesManager.getUsername((AuthorizedAppCompatActivity) getActivity()));
        loadPicture();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        final ListView listView = (ListView) view.findViewById(R.id.preferences_list_view);
        final Button logoutButton = (Button) view.findViewById(R.id.logout_button);

        username = (EditText) view.findViewById(R.id.username);
        imageButton = (ImageButton) view.findViewById(R.id.profile_picture);
        adapter = new PreferencesAdapter(this, android.R.layout.simple_list_item_1, healthPreferences, dietPreferences, allPreferences);

        setUserName(username);

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                UserPreferencesManager.setUsername((AuthorizedAppCompatActivity) getActivity(), s.toString());
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logOut();
                Intent intent = new Intent(mainActivity.getApplicationContext(), WelcomeActivity.class);
                startActivityForResult(intent, 1);
                mainActivity.finish();
            }
        });
        if (!mainActivity.isLogged) {
            logoutButton.setVisibility(View.INVISIBLE);
        }
        listView.setAdapter(adapter);
        refreshUserData();
        setLoadPictureButtonListener(imageButton);
        return view;
    }

    private void setUserName(final EditText username) {
        username.setText(UserPreferencesManager.getUsername((AuthorizedAppCompatActivity) getActivity()));
        if(authorizationModel.getFacebookId() != null) {
            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/" + authorizationModel.getFacebookId(),null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            FacebookUserModel model = new Gson().fromJson(response.getJSONObject().toString(),FacebookUserModel.class);
                            username.setText(model.getName());
                        }
                    }
            ).executeAsync();
        }
    }

    private void loadPicture() {
        String photoPath = "https://maxcdn.icons8.com/Share/icon/Users//edit_user1600.png";
        String memoryPhotoPath = UserPreferencesManager.getProfilePhotoPath(this.getActivity());
        if (!memoryPhotoPath.equals("")) {
            photoPath = memoryPhotoPath;
        } else if (authorizationModel.getFacebookId() != null) {
            photoPath = "https://graph.facebook.com/" + authorizationModel.getFacebookId() + "/picture?type=large";
        }

        Picasso.with(this.getActivity()).load(photoPath).into(imageButton, new Callback() {
            @Override
            public void onSuccess() {}

            @Override
            public void onError() {
                Picasso.with(getActivity())
                        .load(new File(UserPreferencesManager.getProfilePhotoPath(getActivity())))
                        .into(imageButton, new Callback() {
                            @Override
                            public void onSuccess() {
                                imageButton.setScaleType(ImageButton.ScaleType.CENTER_CROP);
                            }

                            @Override
                            public void onError() {}
                        });
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(ARG_HEALTH_PREFERENCES_LIST, (ArrayList<Preference>) healthPreferences);
        outState.putParcelableArrayList(ARG_DIET_PREFERENCES_LIST, (ArrayList<Preference>) dietPreferences);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (isScheduledRefresh) {
            refreshUserPreferences();
        }
    }

    public void setLoadPictureButtonListener(ImageButton button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, RESULT_LOAD_IMAGE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            final Uri selectedImage = data.getData();
            final String[] filePathColumn = {MediaStore.Images.Media.DATA};

            final Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            if (cursor == null) return;
            cursor.moveToFirst();

            final int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            final String picturePath = cursor.getString(columnIndex);
            cursor.close();

            UserPreferencesManager.setProfilePhotoPath((AuthorizedAppCompatActivity) getActivity(), picturePath);
            loadPicture();
        }
    }

    public void refreshUserPreferences() {
        if (getActivity() == null) {
            isScheduledRefresh = true;
        } else {
            refreshUserData();
            healthPreferences = UserPreferencesManager.getHealthPreferences(getActivity());
            dietPreferences = UserPreferencesManager.getDietPreferences(getActivity());
            adapter.refreshUserPreferences(healthPreferences, dietPreferences);
            isScheduledRefresh = false;
            Log.d(getClass().getSimpleName(), "Refreshing user profile fragment with new preferences");
        }
    }

    public void saveUserPreferences() {
        UserPreferencesManager.savePreferences((MainActivity) getActivity(), healthPreferences, dietPreferences);
    }
}
