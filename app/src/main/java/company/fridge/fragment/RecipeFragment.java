package company.fridge.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import company.fridge.activity.RecipeSwipeActivity;
import company.fridge.adapter.RecipeIngredientsAdapter;
import company.fridge.model.Recipe;
import company.fridge.db.RecipesDataSource;
import company.fridge.manager.SavedRecipesManager;
import company.fridge.R;
import lombok.Data;

public class RecipeFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private ImageButton saveButton;
    private Recipe recipe;
    private int sectionNumber;
    private RecipesDataSource recipesDataSource;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            sectionNumber = savedInstanceState.getInt(ARG_SECTION_NUMBER);
        } else {
            sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        recipesDataSource = ((RecipeSwipeActivity) getActivity()).getRecipesDataSource();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(ARG_SECTION_NUMBER, sectionNumber);
        super.onSaveInstanceState(outState);
    }

    public static RecipeFragment newInstance(int sectionNumber) {
        final Bundle arguments = new Bundle();
        arguments.putInt(ARG_SECTION_NUMBER, sectionNumber);

        final RecipeFragment fragment = new RecipeFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.placeholder_recipe, container, false);

        final RecipeSwipeActivity swipeActivity = (RecipeSwipeActivity) getActivity();

        this.recipe = swipeActivity.getRecipeByIndex(sectionNumber);

        final TextView dishNameTextView = (TextView) rootView.findViewById(R.id.dish_name_text_view);
        dishNameTextView.setText(recipe.getLabel());

        final ImageView dishImage = (ImageView) rootView.findViewById(R.id.dish_image_view);
        Picasso.with(swipeActivity).load(recipe.getImage()).into(dishImage);

        final TextView saveLabel = (TextView) rootView.findViewById(R.id.number_of_saved);
        String users = recipe.getNumOfDownloads() == 1 ? " user" : " users";
        saveLabel.setText("Saved by " + recipe.getNumOfDownloads() + users);

        dishImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(recipe.getUrl()); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        final ListView ingredientsListView = (ListView) rootView.findViewById(R.id.ingredients_list);

        final RecipeIngredientsAdapter adapter
                = new RecipeIngredientsAdapter(swipeActivity, R.layout.recipe_ingredient_cell, recipe.getIngredients());
        ingredientsListView.setAdapter(adapter);
        setListViewHeightBasedOnChildren(ingredientsListView, adapter);

        this.saveButton = (ImageButton) rootView.findViewById(R.id.save_button);

        if (recipesDataSource == null) {
            recipesDataSource = ((RecipeSwipeActivity) getActivity()).getRecipesDataSource();
        }

        if (recipesDataSource != null) {
            if (recipesDataSource.isAlreadySaved(recipe)) {
                saveButton.setColorFilter(Color.parseColor("#D3D3D3"));
            } else {
                saveButton.setColorFilter(Color.parseColor("#000000"));
            }
            final ButtonHolder buttonHolder = new ButtonHolder();
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!buttonHolder.isBlocked()) {
                        if (recipesDataSource.isAlreadySaved(recipe)) {
                            new SavedRecipesManager().deleteRecipe(recipesDataSource, recipe, swipeActivity, buttonHolder);
                            saveButton.setColorFilter(Color.parseColor("#000000"));
                            Toast.makeText(getContext(), "Recipe deleted", Toast.LENGTH_LONG).show();
                        } else {
                            new SavedRecipesManager().saveRecipe(recipesDataSource, recipe, swipeActivity, buttonHolder);
                            saveButton.setColorFilter(Color.parseColor("#D3D3D3"));
                            Toast.makeText(getContext(), "Recipe saved", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        recipesDataSource = ((RecipeSwipeActivity) getActivity()).getRecipesDataSource();
    }

    private void setListViewHeightBasedOnChildren(ListView listView, ListAdapter listAdapter) {
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + 3;
        listView.setLayoutParams(params);
    }

    @Data
    public class ButtonHolder {
        private boolean blocked = false;
    }
}