package company.fridge.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import company.fridge.model.AuthorizationModel;
import company.fridge.manager.InternetConnectionManager;
import company.fridge.R;
import company.fridge.service.AsyncCaller;
import company.fridge.service.AsyncJsonTask;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class SignupActivity extends AppCompatActivity implements AsyncCaller {

    private static final String TAG = "SignupActivity";
    private static final String REGISTER_URL_ADDRESS = "https://if-fridge.herokuapp.com/security/register";
    private static final Map<String, String> SIGNUP_HEADER_MAP = ImmutableMap.of("Accept", "application/json", "content-type", "application/x-www-form-urlencoded");
    private static final int REGISTER_SUCCESSFULLY_CODE = 200;
    private static final int EMAIL_IS_ALREADY_USED = 400;
    private static final int REGISTER_SUCCESFULLY = 1;

    private EditText emailText;
    private EditText passwordText;
    private Button signupButton;
    private SignupActivity context;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        emailText = (EditText) findViewById(R.id.input_email);
        passwordText = (EditText) findViewById(R.id.input_password);
        signupButton = (Button) findViewById(R.id.btn_signup);
        context = this;

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetConnectionManager.isNetworkConnected(SignupActivity.this)) signup();
            }
        });
    }

    public void signup() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        signupButton.setEnabled(false);

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("email", email));
        nameValuePairs.add(new BasicNameValuePair("password", password));
        JSONObject jsonObject = new JSONObject();
        for (NameValuePair valuePair : nameValuePairs) {
            try {
                jsonObject.put(valuePair.getName(), valuePair.getValue());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        progressDialog = new ProgressDialog(SignupActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        if (!validate()) {
            onSignupFailed();
        } else {
            AsyncJsonTask.getPostTask(context, 200, REGISTER_URL_ADDRESS, jsonObject, SIGNUP_HEADER_MAP).execute();
            onSignupSuccess();
        }
    }

    public void onSignupSuccess() {
        signupButton.setEnabled(true);
//        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        progressDialog.dismiss();
        signupButton.setEnabled(true);
        Toast.makeText(getBaseContext(), "Registration failed", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    public boolean validate() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("Enter a valid email address");
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 6 || password.length() > 16) {
            passwordText.setError("Between 6 and 16 alphanumeric characters");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }

    @Override
    public void onBackgroundTaskCompleted(int requestCode, Object result) {
        if (requestCode == REGISTER_SUCCESSFULLY_CODE) {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            onSignupSuccess();
                            progressDialog.dismiss();
                        }
                    }, 3000);
            Toast.makeText(getBaseContext(), "Register successfully", Toast.LENGTH_LONG).show();
            String content = (String) result;
            AuthorizationModel model = new Gson().fromJson(content, AuthorizationModel.class);
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra(AuthorizedAppCompatActivity.ARG_AUTHORIZATION_TOKEN, model.getToken());
            intent.putExtra(AuthorizedAppCompatActivity.ARG_FACEBOOK_ID, model.getFacebookId());
            intent.putExtra(AuthorizedAppCompatActivity.ARG_PROFILE_ID, model.getId());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivityForResult(intent, REGISTER_SUCCESFULLY);
            finish();
            return;
        } else if (requestCode == EMAIL_IS_ALREADY_USED) {
            Toast.makeText(getBaseContext(), "Email is already used", Toast.LENGTH_LONG).show();
            return;
        }
        onSignupFailed();
    }
}