package company.fridge.activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import company.fridge.db.RecipeDO;


public class RecipeWebViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        open((RecipeDO) getIntent().getExtras().get("recipe"));
    }

    public void open(RecipeDO recipe) {
        final WebView webView = new WebView(this);
        if (isDeviceOnline()) {
            webView.loadUrl(recipe.getUrl());
            webView.getSettings().setJavaScriptEnabled(true);
        } else {
            webView.loadData(recipe.getContent(), "", null);
        }
        setContentView(webView);
    }

    private boolean isDeviceOnline() {
        final ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected();
    }
}
