package company.fridge.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import company.fridge.db.RecipesDataSource;
import company.fridge.fragment.IngredientsFragment;
import company.fridge.fragment.SavedRecipesFragment;
import company.fridge.fragment.UserProfileFragment;
import company.fridge.manager.PermissionManager;
import company.fridge.manager.SavedRecipesManager;
import company.fridge.manager.UserPreferencesManager;
import company.fridge.R;
import lombok.Data;

@Data
public class MainActivity extends AuthorizedAppCompatActivity {
    private RecipesDataSource recipesDataSource;
    private boolean appOpened = false;
    public boolean isLogged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        final ViewPagerAdapter viewPagerAdapter = setupViewPager(viewPager);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.user);
        tabLayout.getTabAt(1).setIcon(R.drawable.fridge);
        tabLayout.getTabAt(2).setIcon(R.drawable.save);

        if (savedInstanceState == null) {
            UserPreferencesManager.resetUserType();
            PermissionManager.verifyStoragePermissions(this);
            UserPreferencesManager.getLabelsFromServer(this);
            SavedRecipesManager.updateSavedRecipes(this);
        }
        recipesDataSource = new RecipesDataSource(this);

        getSharedPreferences(UserPreferencesManager.USER_PREFERENCES, 0).registerOnSharedPreferenceChangeListener(
                new SharedPreferences.OnSharedPreferenceChangeListener() {
                    @Override
                    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                        Log.d(MainActivity.class.getSimpleName(), "Detected Shared Preferences change, updating views");
                        if (viewPagerAdapter.getItem(0) != null) {
                            ((UserProfileFragment) viewPagerAdapter.getItem(0)).refreshUserPreferences();
                        }
                    }
                });
        if(super.getProfileId() != null) {
            isLogged = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (recipesDataSource == null) {
            recipesDataSource = new RecipesDataSource(this);
        }
        recipesDataSource.open();
    }

    @Override
    protected void onPause() {
        super.onPause();
        recipesDataSource.close();
    }

    private ViewPagerAdapter setupViewPager(ViewPager viewPager) {
        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new UserProfileFragment());
        adapter.addFragment(new IngredientsFragment());
        adapter.addFragment(new SavedRecipesFragment());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(1);
        return adapter;
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        void addFragment(Fragment fragment) {
            fragmentList.add(fragment);
        }
    }

    public RecipesDataSource getRecipesDataSource() {
        if (recipesDataSource == null) {
            recipesDataSource = new RecipesDataSource(this);
        }
        recipesDataSource.open();

        return recipesDataSource;
    }
}
