package company.fridge.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import company.fridge.model.AuthorizationModel;
import company.fridge.model.UserModel;
import company.fridge.manager.InternetConnectionManager;
import company.fridge.R;
import company.fridge.service.AsyncCaller;
import company.fridge.service.AsyncJsonTask;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class LoginActivity extends AppCompatActivity implements AsyncCaller {

    private static final String LOGIN_URL_ADDRESS = "https://if-fridge.herokuapp.com/security/login";
    private static final Map<String, String> LOGIN_HEADER_MAP = ImmutableMap.of("Accept", "application/json", "content-type", "application/x-www-form-urlencoded");
    private static final int LOGIN_SUCCESFULLY = 1;

    private EditText emailText;
    private EditText passwordText;
    private Button loginButton;
    private LoginActivity context;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_login);

        emailText = (EditText) findViewById(R.id.email_edit_text);
        passwordText = (EditText) findViewById(R.id.password_edit_text);
        loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetConnectionManager.isNetworkConnected(LoginActivity.this)) {
                    login();
                }
            }
        });
    }

    public static Map<String, String> getLoginHeaderMap() {
        return LOGIN_HEADER_MAP;
    }

    private void login() {
        final UserModel user = new UserModel(emailText.getText().toString(), passwordText.getText().toString());
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("email", user.getEmail()));
        nameValuePairs.add(new BasicNameValuePair("password", user.getPassword()));
        JSONObject jsonObject = new JSONObject();
        for (NameValuePair valuePair : nameValuePairs) {
            try {
                jsonObject.put(valuePair.getName(), valuePair.getValue());
            } catch (JSONException e) {

            }
        }

        loginButton.setEnabled(false);
        progressDialog = new ProgressDialog(LoginActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        if (!validate()) {
            onSignupFailed();
        } else {
            AsyncJsonTask.getPostTask(context, 200, LOGIN_URL_ADDRESS, jsonObject, LOGIN_HEADER_MAP).execute();
        }
    }

    private void onLoginSuccess() {
        loginButton.setEnabled(true);
        finish();
    }

    public void onSignupFailed() {
        progressDialog.dismiss();
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_SHORT).show();
        loginButton.setEnabled(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackgroundTaskCompleted(int requestCode, Object result) {
        if (requestCode == 200) {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            onLoginSuccess();
                            progressDialog.dismiss();
                        }
                    }, 3000);

            String content = (String) result;
            AuthorizationModel model = new Gson().fromJson(content, AuthorizationModel.class);
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra(AuthorizedAppCompatActivity.ARG_AUTHORIZATION_TOKEN, model.getToken());
            intent.putExtra(AuthorizedAppCompatActivity.ARG_FACEBOOK_ID, model.getFacebookId());
            intent.putExtra(AuthorizedAppCompatActivity.ARG_PROFILE_ID, model.getId());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivityForResult(intent, LOGIN_SUCCESFULLY);
            ActivityCompat.finishAffinity(this);
            return;
        }
        onSignupFailed();
    }

    public boolean validate() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("Enter a valid email address");
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 6 || password.length() > 16) {
            passwordText.setError("Between 6 and 16 alphanumeric characters");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }
}
