package company.fridge.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.animation.Animation;

import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.List;

import company.fridge.model.Recipe;
import company.fridge.fragment.RecipeFragment;
import company.fridge.model.RecipeUrlData;
import company.fridge.db.RecipesDataSource;
import company.fridge.fetcher.RecipesResponseFetcher;
import company.fridge.fragment.IngredientsFragment;
import company.fridge.manager.UserPreferencesManager;
import company.fridge.R;
import lombok.Data;

@Data
public class RecipeSwipeActivity extends AuthorizedAppCompatActivity implements Animation.AnimationListener {

    private static final String ARG_RECIPES_LIST = "recipes list";

    private SectionsPagerAdapter sectionsPagerAdapter;
    private RecipesDataSource recipesDataSource;
    private RecipeUrlData urlData;
    private List<Recipe> recipes;
    private List<String> selectedIngredients = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_swipe);

        this.selectedIngredients = getIntent().getStringArrayListExtra(IngredientsFragment.ARG_SELECTED_INGREDIENTS);
        this.recipesDataSource = new RecipesDataSource(getApplicationContext());
        this.sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        final ViewPager container = (ViewPager) findViewById(R.id.container);
        container.setAdapter(sectionsPagerAdapter);

        if (savedInstanceState != null) setRecipes(savedInstanceState.<Recipe>getParcelableArrayList(ARG_RECIPES_LIST));
        else {
            urlData = new RecipeUrlData(selectedIngredients, 0, 10, 0, 0, UserPreferencesManager.getCheckedHealthPreferences(this),
                    UserPreferencesManager.getCheckedDietPreferences(this));
            new RecipesResponseFetcher(this, getAuthorizationToken()).execute();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (recipesDataSource == null) {
            recipesDataSource = new RecipesDataSource(this);
        }
        recipesDataSource.open();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (recipesDataSource != null) {
            recipesDataSource.close();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(ARG_RECIPES_LIST, (ArrayList<Recipe>) recipes);
        super.onSaveInstanceState(outState);
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
        sectionsPagerAdapter.setRecipes(recipes);
    }

    public RecipesDataSource getRecipesDataSource() {
        if (recipesDataSource == null) {
            recipesDataSource = new RecipesDataSource(this);
            recipesDataSource.open();
        }
        return recipesDataSource;
    }

    public Recipe getRecipeByIndex(int recipeIndex) {
        return recipes.get(recipeIndex);
    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    @Data
    private class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private List<Recipe> recipes;

        private SectionsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        private void setRecipes(List<Recipe> recipes) {
            this.recipes = recipes;
            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int position) {
            return RecipeFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            if (!Objects.equal(recipes, null)) return recipes.size();
            else return 0;
        }
    }
}