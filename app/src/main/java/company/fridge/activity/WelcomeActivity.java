package company.fridge.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import company.fridge.model.AuthorizationModel;
import company.fridge.R;
import company.fridge.service.AsyncCaller;
import company.fridge.service.AsyncJsonTask;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class WelcomeActivity extends AuthorizedAppCompatActivity implements AsyncCaller {

    private static final String FACEBOOK_URL_ADDRESS = "https://if-fridge.herokuapp.com/security/facebook";
    private static final int LOGIN_SUCCESFULLY = 1;
    private static final int REQUEST_SIGNUP = 0;

    private Button loginButton;
    private Button signupButton;
    private Button withoutLoginButton;
    private LoginButton facebookButton;
    private CallbackManager callbackManager;
    private WelcomeActivity welcomeActivity;
    private String FacebookId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        welcomeActivity = this;
        FacebookSdk.sdkInitialize(welcomeActivity);
        setContentView(R.layout.activity_welcome);

        callbackManager = CallbackManager.Factory.create();
        signupButton = (Button) findViewById(R.id.create_account_button);
        loginButton = (Button) findViewById(R.id.login_button);
        withoutLoginButton = (Button) findViewById(R.id.btn_without_login);
        facebookButton = (LoginButton) findViewById(R.id.btn_facebook);

        enableButtons(false);

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the Login activity
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });

        facebookButton.setReadPermissions("email");
        facebookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginByFacebook(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {}

            @Override
            public void onError(FacebookException e) {}
        });
        withoutLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivityForResult(intent, RESULT_OK);
                finish();
            }
        });

        if(isLoggedIn()) {
            loginByFacebook(AccessToken.getCurrentAccessToken());
        } else {
            enableButtons(true);
        }
    }

    private void enableButtons(boolean isEnabled) {
        loginButton.setEnabled(isEnabled);
        signupButton.setEnabled(isEnabled);
        withoutLoginButton.setEnabled(isEnabled);
        facebookButton.setEnabled(isEnabled);
    }

    private void loginByFacebook(AccessToken accessToken) {
        FacebookId = accessToken.getUserId();
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("token", accessToken.getToken()));
        JSONObject jsonObject = new JSONObject();
        for (NameValuePair valuePair : nameValuePairs) {
            try {
                jsonObject.put(valuePair.getName(), valuePair.getValue());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        AsyncJsonTask.getPostTask(welcomeActivity, 200, FACEBOOK_URL_ADDRESS, jsonObject, LoginActivity.getLoginHeaderMap()).execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackgroundTaskCompleted(int requestCode, Object result) {
        if (requestCode == 200) {
            new android.os.Handler().postDelayed(new Runnable() {
                        public void run() {
                            onLoginSuccess();
                        }
            }, 3000);

            String content = (String) result;
            AuthorizationModel model = new Gson().fromJson(content, AuthorizationModel.class);
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra(AuthorizedAppCompatActivity.ARG_PROFILE_ID, model.getId());
            intent.putExtra(AuthorizedAppCompatActivity.ARG_AUTHORIZATION_TOKEN, model.getToken());
            intent.putExtra(AuthorizedAppCompatActivity.ARG_FACEBOOK_ID, FacebookId);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivityForResult(intent, LOGIN_SUCCESFULLY);
            ActivityCompat.finishAffinity(this);
            return;
        }
        onSignupFailed();
    }

    private void onLoginSuccess() {
        loginButton.setEnabled(true);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }
}
