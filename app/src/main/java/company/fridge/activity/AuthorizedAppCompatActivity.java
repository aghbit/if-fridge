package company.fridge.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import lombok.Data;

@Data
public abstract class AuthorizedAppCompatActivity extends AppCompatActivity {
    public static String ARG_PROFILE_ID = "id";
    public static String ARG_AUTHORIZATION_TOKEN = "token";
    public static String ARG_FACEBOOK_ID = "facebook";

    private String authorizationToken;
    private String profileId;
    private String facebookId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        authorizationToken = getIntent().getStringExtra(ARG_AUTHORIZATION_TOKEN);
        profileId = getIntent().getStringExtra(ARG_PROFILE_ID);
        facebookId = getIntent().getStringExtra(ARG_FACEBOOK_ID);
    }
}
