package company.fridge.manager;


import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import company.fridge.activity.AuthorizedAppCompatActivity;
import company.fridge.activity.MainActivity;
import company.fridge.model.Preference;
import company.fridge.model.UserPreferencesPostModel;
import company.fridge.fetcher.LabelResponseFetcher;
import company.fridge.fetcher.UserDataResponseFetcher;
import company.fridge.fetcher.UserPreferencesResponseFetcher;
import company.fridge.service.AsyncCaller;
import company.fridge.service.AsyncJsonTask;
import lombok.Data;

@Data
public class UserPreferencesManager implements AsyncCaller {

    public static final String USER_PREFERENCES = "user preferences";
    private static final String PROFILE_PHOTO_PATH = "profile photo path";
    private static final String TIMESTAMP_PREF = "timestamp pref";
    public static final String PROFILE_ID_PREF = "profile id pref";
    public static final String PROFILE_ID_NULL = "profile id null";
    private static final String USERNAME = "username";

    private static final String HEALTH_PREFS = "health prefs";
    private static final String DIET_PREFS = "diet prefs";

    private static final String USER_PREFS_URL = "https://if-fridge.herokuapp.com/app/user/prefs";
    private static final String USER_DATA_URL = "https://if-fridge.herokuapp.com/app/user/data";

    private static USER_TYPE userType;

    public static void resetUserType() {
        userType = null;
    }

    enum USER_TYPE {
        NEW_UNREGISTERED_USER,
        OLD_UNREGISTERED_USER,
        NEW_REGISTERED_USER,
        OLD_REGISTERED_USER;
    }

    static USER_TYPE getUserType(AuthorizedAppCompatActivity activity) {
        if (userType == null) {
            userType = _getUserType(activity);
        }
        return userType;
    }

    private static USER_TYPE _getUserType(AuthorizedAppCompatActivity activity) {
        String sharedPreferencesProfileId = activity.getSharedPreferences(USER_PREFERENCES, 0).getString(PROFILE_ID_PREF, PROFILE_ID_NULL);
        String appProfileId = activity.getProfileId() == null ? PROFILE_ID_NULL : activity.getProfileId();

        if (appProfileId.equals(PROFILE_ID_NULL)) {
            // Not registered.
            if (sharedPreferencesProfileId.equals(PROFILE_ID_NULL)) {
                // Last update for unregistered user.
                return USER_TYPE.OLD_UNREGISTERED_USER;
            } else {
                // Last update for registered user.
                return USER_TYPE.NEW_UNREGISTERED_USER;
            }
        } else {
            // Registered.
            if (appProfileId.equals(sharedPreferencesProfileId)) {
                // Last update for user with the same id.
                return USER_TYPE.OLD_REGISTERED_USER;
            } else {
                // Last update for user with different id.
                return USER_TYPE.NEW_REGISTERED_USER;
            }
        }
    }

    public static void getLabelsFromServer(AuthorizedAppCompatActivity activity) {
        userType = getUserType(activity);
        if (userType == USER_TYPE.OLD_REGISTERED_USER || userType == USER_TYPE.NEW_REGISTERED_USER) {
            // Gets diet and health labels from server.
            new LabelResponseFetcher(activity).execute();
            new UserDataResponseFetcher(activity, USER_DATA_URL, getHeaders(activity.getAuthorizationToken())).execute();
        } else if (userType == USER_TYPE.NEW_UNREGISTERED_USER) {
            // Delete everything from database if there is other user's data in the database.
            updateUserPreferencesFromServer(activity, new ArrayList<String>(), new ArrayList<String>(), new Date().getTime());
        }
        if (userType == USER_TYPE.NEW_REGISTERED_USER || userType == USER_TYPE.NEW_UNREGISTERED_USER) {
            setProfilePhotoPath(activity, "");
            setUsername(activity, "");
            new UserDataResponseFetcher(activity, USER_DATA_URL, getHeaders(activity.getAuthorizationToken())).execute();
        }
    }

    public static void getUserPreferencesFromServer(AuthorizedAppCompatActivity activity) {
        // Gets diet and health labels from server.
        new UserPreferencesResponseFetcher(activity, USER_PREFS_URL, getHeaders(activity.getAuthorizationToken())).execute();
    }

    private static void updatePreferencesList(SharedPreferences preferences, List<String> labels) {
        if (labels != null && !labels.isEmpty()) {
            for (String label : labels) {
                if (!preferences.getAll().keySet().contains(label.toUpperCase())) {
                    preferences.edit().putBoolean(label.toUpperCase(), false).apply();
                }
            }
            for (String pref : preferences.getAll().keySet()) {
                if (!labels.contains(pref.toLowerCase())) {
                    preferences.edit().remove(pref.toUpperCase()).apply();
                }
            }
        }
        preferences.edit().commit();
    }

    private static boolean savePreferences(SharedPreferences preferences, List<Preference> userPreferences) {
        boolean changed = false;
        if (userPreferences != null && !userPreferences.isEmpty()) {
            for (Preference userPreference : userPreferences) {
                changed = changed || preferences.getBoolean(userPreference.getName(), false) != userPreference.getValue();
                preferences.edit().putBoolean(userPreference.getName(), userPreference.getValue()).apply();
            }
        }
        return changed;
    }

    public static void updatePreferencesList(Activity activity, List<String> healthLabels, List<String> dietLabels) {
        updatePreferencesList(activity.getSharedPreferences(HEALTH_PREFS, 2), healthLabels);
        updatePreferencesList(activity.getSharedPreferences(DIET_PREFS, 2), dietLabels);
    }

    public static void savePreferences(MainActivity activity, List<Preference> healthPreferences, List<Preference> dietPreferences) {
        boolean healthPrefsChanged = savePreferences(activity.getSharedPreferences(HEALTH_PREFS, 2), healthPreferences);
        boolean dietPrefsChanged = savePreferences(activity.getSharedPreferences(DIET_PREFS, 2), dietPreferences);

        if (healthPrefsChanged || dietPrefsChanged) {
            savePreferencesOnServer(activity);
            updateTimestamp(activity);
        }
    }

    private static void updateTimestamp(AuthorizedAppCompatActivity activity) {
        activity.getSharedPreferences(USER_PREFERENCES, 2).edit().putLong(TIMESTAMP_PREF, new Date().getTime()).apply();
    }

    private static void updateTimestamp(AuthorizedAppCompatActivity activity, Long timestamp) {
        activity.getSharedPreferences(USER_PREFERENCES, 2).edit().putLong(TIMESTAMP_PREF, timestamp).apply();
    }

    private static void savePreferencesOnServer(MainActivity activity) {
        new UserPreferencesManager().sendUpdatedUserPreferencesToServer(getCheckedHealthPreferences(activity), getCheckedDietPreferences(activity), activity.getAuthorizationToken());
    }

    private static void updateUserPreferencesFromServer(SharedPreferences preferences, List<String> serverPrefs) {
        for (String pref : preferences.getAll().keySet()) {
            if (serverPrefs.contains(pref.toLowerCase()) || serverPrefs.contains(pref.toUpperCase())) {
                preferences.edit().putBoolean(pref.toUpperCase(), true).apply();
            } else {
                preferences.edit().putBoolean(pref.toUpperCase(), false).apply();
            }
        }
    }

    public static void updateUserPreferencesFromServer(AuthorizedAppCompatActivity activity, List<String> healthPreferences, List<String> dietPreferences, long
            timestamp) {
        USER_TYPE userType = getUserType(activity);
        if (userType == USER_TYPE.NEW_UNREGISTERED_USER || userType == USER_TYPE.NEW_REGISTERED_USER
                || (userType == USER_TYPE.OLD_REGISTERED_USER
                && activity.getSharedPreferences(USER_PREFERENCES, 0).getLong(TIMESTAMP_PREF, 0) < timestamp)) {
            updateUserPreferencesFromServer(activity.getSharedPreferences(HEALTH_PREFS, 2), healthPreferences);
            updateUserPreferencesFromServer(activity.getSharedPreferences(DIET_PREFS, 2), dietPreferences);
            updateTimestamp(activity, timestamp);
            Log.d(UserPreferencesManager.class.getSimpleName(), "Preferences updated to newer ones");
        }
        activity.getSharedPreferences(USER_PREFERENCES, 2).edit().putString(PROFILE_ID_PREF, activity.getProfileId() == null ?
                PROFILE_ID_NULL : activity.getProfileId()).apply();
    }

    public static List<Preference> getHealthPreferences(Activity activity) {
        if (activity != null) {
            if (activity.getSharedPreferences(HEALTH_PREFS, 0).getAll().isEmpty()) {
                return new ArrayList<>(Arrays.asList(new Preference("GLUTEN FREE", true), new Preference("NUT FREE", false)));
            } else {
                List<Preference> healthPreferences = new ArrayList<>();
                SharedPreferences preferences = activity.getSharedPreferences(HEALTH_PREFS, 0);
                for (String key : preferences.getAll().keySet()) {
                    healthPreferences.add(new Preference(key, preferences.getBoolean(key, false)));
                }
                return healthPreferences;
            }
        }
        return new ArrayList<>();
    }

    public static List<Preference> getDietPreferences(Activity activity) {
        if (activity.getSharedPreferences(DIET_PREFS, 0).getAll().isEmpty()) {
            return new ArrayList<>(Arrays.asList(new Preference("BALANCED", true), new Preference("LOW-CARB", false)));
        } else {
            List<Preference> dietPreferences = new ArrayList<>();
            SharedPreferences preferences = activity.getSharedPreferences(DIET_PREFS, 0);
            for (String key : preferences.getAll().keySet()) {
                dietPreferences.add(new Preference(key, preferences.getBoolean(key, false)));
            }
            return dietPreferences;
        }
    }

    private static List<String> getCheckedPreferences(SharedPreferences preferences) {
        List<String> healthPreferences = new ArrayList<>();
        for (String key : preferences.getAll().keySet()) {
            if (preferences.getBoolean(key, false)) {
                healthPreferences.add(key.toLowerCase());
            }
        }
        return healthPreferences;
    }

    public static List<String> getCheckedHealthPreferences(Activity activity) {
        return getCheckedPreferences(activity.getSharedPreferences(HEALTH_PREFS, 0));
    }

    public static List<String> getCheckedDietPreferences(Activity activity) {
        return getCheckedPreferences(activity.getSharedPreferences(DIET_PREFS, 0));
    }

    public static String getUsername(AuthorizedAppCompatActivity activity) {
        return activity.getSharedPreferences(USER_PREFERENCES, 0).getString(USERNAME, "");
    }

    public static void setUsername(AuthorizedAppCompatActivity activity, String name) {
        String oldUserName = activity.getSharedPreferences(USER_PREFERENCES, 0).getString(USERNAME, "");
        if (!name.equals(oldUserName)) {
            activity.getSharedPreferences(USER_PREFERENCES, 2).edit().putString(USERNAME, name).apply();
            if (!name.equals("")) {
                UserDataManager userDataManager = new UserDataManager();
                userDataManager.sendUpdatedUserDataToServer(name, activity.getSharedPreferences(USER_PREFERENCES, 0)
                        .getString(PROFILE_PHOTO_PATH, ""), USER_DATA_URL, activity.getAuthorizationToken());
                updateTimestamp(activity);
            }
        }
    }

    public static String getProfilePhotoPath(Activity activity) {
        return activity.getSharedPreferences(USER_PREFERENCES, 0).getString(PROFILE_PHOTO_PATH, "");
    }

    public static void setProfilePhotoPath(AuthorizedAppCompatActivity activity, String path) {
        String oldPhotoPath = activity.getSharedPreferences(USER_PREFERENCES, 0).getString(PROFILE_PHOTO_PATH, "");
        if (!path.equals(oldPhotoPath)) {
            activity.getSharedPreferences(USER_PREFERENCES, 2).edit().putString(PROFILE_PHOTO_PATH, path).apply();
            FilesManager.deletePhotoFromMemory(activity, oldPhotoPath);
            if (!path.equals("")) {
                UserDataManager userDataManager = new UserDataManager();
                userDataManager.sendUpdatedUserDataToServer(activity.getSharedPreferences(USER_PREFERENCES, 0).getString(USERNAME, ""),
                        path, USER_DATA_URL, activity.getAuthorizationToken());
                updateTimestamp(activity);
            }
        }
    }

    private void sendUpdatedUserPreferencesToServer(List<String> healthPreferences, List<String> dietPreferences, String token) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject = new JSONObject(new Gson().toJson(new UserPreferencesPostModel(healthPreferences, dietPreferences)));
        } catch (JSONException e) {
            Log.d(getClass().getSimpleName(), "Error while parsing JSON");
        }

        Log.d(getClass().getSimpleName(), jsonObject.toString());
        Map<String, String> headers = getHeaders(token);
        AsyncJsonTask.getPostTask(this, 200, USER_PREFS_URL, jsonObject, headers).execute();
    }

    @Override
    public void onBackgroundTaskCompleted(int requestCode, Object result) {
        if (requestCode == 200) {
            Log.d(getClass().getSimpleName(), "Successfully updated user prefs on server");
        } else {
            Log.d(getClass().getSimpleName(), "Couldn't update user prefs on server");
        }
    }

    private static Map<String, String> getHeaders(String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", token);
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        return headers;
    }
}
