package company.fridge.manager;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;


public class InternetConnectionManager {

    public static boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isConnected = cm.getActiveNetworkInfo() != null;
        if (!isConnected) {
            Toast.makeText(activity, "Internet is not connected!", Toast.LENGTH_SHORT).show();
        }

        return isConnected;
    }
}
