package company.fridge.manager;


import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import company.fridge.service.AsyncCaller;
import company.fridge.service.AsyncJsonTaskForUserData;

public class UserDataManager implements AsyncCaller {

    @Override
    public void onBackgroundTaskCompleted(int requestCode, Object result) {
        if (requestCode == 200) {
            Log.d(getClass().getSimpleName(), "Successfully updated user photo and nickname on server");

        } else {
            Log.d(getClass().getSimpleName(), "Couldn't update user photo and nickname on server");
        }
    }

    public void sendUpdatedUserDataToServer(final String username, String imagePath, final String uri, final String token) {

        if (imagePath == null || imagePath.equals("")) {
            imagePath = "https://maxcdn.icons8.com/Share/icon/Users//edit_user1600.png";
        }
        Log.d(getClass().getSimpleName(), imagePath);
        final AsyncCaller asyncCaller = this;


        new AsyncTask<String, Void, File>() {
            @Override
            protected File doInBackground(String... params) {
                String imagePath = params[0];
                if (imagePath.startsWith("http")) {
                    try {
                        String imageName = imagePath.substring("https://if-fridge.s3.us-east-2.amazonaws.com/profile_images/".length()).replace('/', '-');
                        return FilesManager.downloadPhotoAndGetFile(imagePath, imageName);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    return FilesManager.compressPhotoAndGetFile(imagePath);
                }
                return null;
            }

            @Override
            protected void onPostExecute(File file) {
                super.onPostExecute(file);
                if (file != null) {
                    new AsyncJsonTaskForUserData(asyncCaller, 200, uri, username, file, getHeaders(token)).execute();
                }
            }
        }.execute(imagePath);

    }

    private static Map<String, String> getHeaders(String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", token);
        headers.put("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
        return headers;
    }
}
