package company.fridge.manager;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import company.fridge.db.RecipesDataSource;

public class FilesManager {
    public static String getFilesDirectoryPath() {
        return Environment.getExternalStorageDirectory().toString() + "/Fridge/";
    }

    private static void createFilesDirectory() {
        final File photosDirectory = new File(getFilesDirectoryPath());
        if (!photosDirectory.exists()) {
            photosDirectory.mkdir();
        }
    }

    public static File getOutputFile(String filename) throws IOException {
        createFilesDirectory();
        final File imgFile = new File(getFilesDirectoryPath() + filename);
        if (!imgFile.exists()) {
            imgFile.createNewFile();
        }
        return imgFile;
    }

    public static File downloadPhotoAndGetFile(String imageUrl, String filename) throws IOException {
        final Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(imageUrl).getContent());
        final FileOutputStream out = new FileOutputStream(getOutputFile(filename));
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, out);
        out.flush();
        out.close();
        return new File(getFilesDirectoryPath() + filename);
    }

    public static File compressPhotoAndGetFile(String filePath) {
        try {
            String filename = new File(filePath).getName();
            final Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            final FileOutputStream out = new FileOutputStream(getOutputFile(filename));

            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, out);
            out.flush();
            out.close();

             return new File(getFilesDirectoryPath() + filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void deletePhotoFromMemory(Context context, String photoPath) {
        try {
            File file = new File(photoPath);
            file.delete();
            if (file.exists()) {
                context.deleteFile(file.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(RecipesDataSource.class.getSimpleName(), "Couldn't delete photo file: " + photoPath);
        }
    }
}
