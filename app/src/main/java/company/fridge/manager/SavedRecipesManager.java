package company.fridge.manager;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import company.fridge.activity.AuthorizedAppCompatActivity;
import company.fridge.activity.MainActivity;
import company.fridge.model.Recipe;
import company.fridge.model.SavedRecipesPostModel;
import company.fridge.db.RecipeDO;
import company.fridge.db.RecipesDataSource;
import company.fridge.fetcher.SavedRecipesResponseFetcher;
import company.fridge.fragment.RecipeFragment;
import company.fridge.service.AsyncCaller;
import company.fridge.service.AsyncJsonTask;

import static company.fridge.manager.UserPreferencesManager.PROFILE_ID_NULL;
import static company.fridge.manager.UserPreferencesManager.PROFILE_ID_PREF;

public class SavedRecipesManager implements AsyncCaller {
    private static final String SAVED_RECIPES_URL = "https://if-fridge.herokuapp.com/app/user/favourite/recipes";
    private static final String SAVED_RECIPES_TIMESTAMP = "saved recipes timestamp";
    private static final String SAVED_RECIPES_PREFS = "saved_recipes";

    public void saveRecipe(RecipesDataSource recipesDataSource, Recipe recipe, AuthorizedAppCompatActivity authorizedAppCompatActivity, RecipeFragment.ButtonHolder buttonHolder) {
        recipesDataSource.saveRecipe(recipe, buttonHolder);

        SharedPreferences preferences = authorizedAppCompatActivity.getSharedPreferences(SAVED_RECIPES_PREFS, 2);
        preferences.edit().putLong(SAVED_RECIPES_TIMESTAMP, new Date().getTime()).apply();
        saveRecipeOnServer(recipe, authorizedAppCompatActivity.getAuthorizationToken());
    }

    public void deleteRecipe(RecipesDataSource recipesDataSource, Recipe recipe, AuthorizedAppCompatActivity authorizedAppCompatActivity, RecipeFragment.ButtonHolder buttonHolder) {
        recipesDataSource.deleteRecipe(recipe, buttonHolder);

        SharedPreferences preferences = authorizedAppCompatActivity.getSharedPreferences(SAVED_RECIPES_PREFS, 2);
        preferences.edit().putLong(SAVED_RECIPES_TIMESTAMP, new Date().getTime()).apply();

        deleteRecipeOnServer(recipe.getUrl(), authorizedAppCompatActivity.getAuthorizationToken());

    }

    public void deleteRecipe(RecipesDataSource recipesDataSource, RecipeDO recipe, AuthorizedAppCompatActivity authorizedAppCompatActivity) {
        recipesDataSource.deleteRecipe(recipe);

        SharedPreferences preferences = authorizedAppCompatActivity.getSharedPreferences(SAVED_RECIPES_PREFS, 2);
        preferences.edit().putLong(SAVED_RECIPES_PREFS, new Date().getTime()).apply();

        deleteRecipeOnServer(recipe.getUrl(), authorizedAppCompatActivity.getAuthorizationToken());
    }

    private static void deleteAllRecipes(RecipesDataSource recipesDataSource, Activity activity) {
        recipesDataSource.deleteAll();
        SharedPreferences preferences = activity.getSharedPreferences(SAVED_RECIPES_PREFS, 2);
        preferences.edit().putLong(SAVED_RECIPES_PREFS, new Date().getTime()).apply();
    }

    public static void updateSavedRecipes(MainActivity mainActivity) {
        UserPreferencesManager.USER_TYPE userType = UserPreferencesManager.getUserType(mainActivity);
        if (userType == UserPreferencesManager.USER_TYPE.NEW_UNREGISTERED_USER) {
            // Delete everything from database if there is other user's data in the database.
            deleteAllRecipes(mainActivity.getRecipesDataSource(), mainActivity);
        }
        if (userType == UserPreferencesManager.USER_TYPE.NEW_REGISTERED_USER
                || userType == UserPreferencesManager.USER_TYPE.OLD_REGISTERED_USER) {
            new SavedRecipesResponseFetcher(mainActivity, mainActivity.getRecipesDataSource(), SAVED_RECIPES_URL,
                    getHeaders(mainActivity.getAuthorizationToken())).execute();
        }
        mainActivity.getSharedPreferences(SAVED_RECIPES_PREFS, 2).edit().putString(PROFILE_ID_PREF, mainActivity.getProfileId()).apply();
    }

    public static void updateSavedRecipesToThoseReceivedFromServer(Long timestamp, List<SavedRecipesPostModel> serverRecipes,
                                                                   RecipesDataSource recipesDataSource, Activity activity, String profileId) {
        SharedPreferences preferences = activity.getSharedPreferences(SAVED_RECIPES_PREFS, 2);
        if (!profileId.equals(preferences.getString(PROFILE_ID_PREF, PROFILE_ID_NULL))
                || timestamp > preferences.getLong(SAVED_RECIPES_PREFS, 0)) {
            // Execute this method if saved recipes stored on the device are of a different user
            // OR if the saved recipes stored on the device are outdated.
            Map<String, SavedRecipesPostModel> recipesMap = new HashMap<>();
            for (SavedRecipesPostModel recipe : serverRecipes) {
                recipesDataSource.saveRecipe(recipe);
                recipesMap.put(recipe.getUri(), recipe);
            }

            List<RecipeDO> databaseRecipes = recipesDataSource.getAllRecipes();
            for (RecipeDO recipe : databaseRecipes) {
                if (!recipesMap.containsKey(recipe.getUrl())) {
                    recipesDataSource.deleteRecipe(recipe);
                }
            }
            preferences.edit().putLong(SAVED_RECIPES_PREFS, timestamp).apply();
            preferences.edit().putString(PROFILE_ID_PREF, profileId).apply();
        }
    }

    private static Map<String, String> getHeaders(String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", token);
        return headers;
    }

    private void saveRecipeOnServer(Recipe recipe, String token) {
        if (token != null) {
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject = new JSONObject(new Gson().toJson(new SavedRecipesPostModel(recipe)));
            } catch (JSONException e) {
                Log.d(getClass().getSimpleName(), "Error while parsing JSON");
            }

            Log.d(getClass().getSimpleName(), jsonObject.toString());
            Map<String, String> headers = getHeaders(token);
            AsyncJsonTask.getPostTask(this, 200, SAVED_RECIPES_URL, jsonObject, headers).execute();
        }
    }

    private void deleteRecipeOnServer(String url, String token) {
        if (token != null) {
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("uri", url);
            } catch (JSONException e) {
                Log.d(getClass().getSimpleName(), "Error while parsing JSON");
            }

            Log.d(getClass().getSimpleName(), jsonObject.toString());
            Map<String, String> headers = getHeaders(token);
            AsyncJsonTask.getDeleteTask(this, 200, SAVED_RECIPES_URL, jsonObject, headers).execute();
        }
    }

    @Override
    public void onBackgroundTaskCompleted(int requestCode, Object result) {
        if (requestCode == 200) {
            Log.d(getClass().getSimpleName(), "Successfully updated saved recipe on server");
        } else {
            Log.d(getClass().getSimpleName(), "Couldn't update saved recipe on server");
        }
    }
}
