package company.fridge.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "recipes.db";

    static final String TABLE_RECIPES = "saved_recipes";
    static final String COLUMN_ID = "_id";
    static final String COLUMN_LABEL = "label";
    static final String COLUMN_URL = "url";
    static final String COLUMN_CONTENT = "content";
    static final String COLUMN_PHOTO_PATH = "photo_path";
    static final String COLUMN_PHOTO_URL = "photo_url";

    private static final String DATABASE_CREATE =
            "create table " + TABLE_RECIPES
                    + "( " + COLUMN_ID  + " integer primary key autoincrement, "
                    + COLUMN_LABEL  + " text not null, "
                    + COLUMN_URL + " text not null, "
                    + COLUMN_CONTENT  + " text not null, "
                    + COLUMN_PHOTO_PATH + " text not null, "
                    + COLUMN_PHOTO_URL + " text not null" + ");";

    SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(SQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECIPES);
        onCreate(db);
    }
}
