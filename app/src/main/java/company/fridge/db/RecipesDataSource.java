package company.fridge.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import company.fridge.model.Recipe;
import company.fridge.model.SavedRecipesPostModel;
import company.fridge.fragment.RecipeFragment;
import company.fridge.fragment.SavedRecipesFragment;
import company.fridge.manager.FilesManager;

import static company.fridge.manager.FilesManager.getFilesDirectoryPath;

public class RecipesDataSource {

    private static SavedRecipesFragment savedRecipesFragment = null;

    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;
    private String[] allColumns = {SQLiteHelper.COLUMN_ID, SQLiteHelper.COLUMN_LABEL, SQLiteHelper.COLUMN_URL,
            SQLiteHelper.COLUMN_CONTENT, SQLiteHelper.COLUMN_PHOTO_PATH, SQLiteHelper.COLUMN_PHOTO_URL};

    private int runningAsyncTasks;
    private boolean waitingToClose;
    private Context context;

    public RecipesDataSource(Context context) {
        dbHelper = new SQLiteHelper(context);
        this.context = context;
        runningAsyncTasks = 0;
        waitingToClose = false;
    }

    public void startAsyncTaskOnDatabase() {
        runningAsyncTasks++;
    }

    public void stopAsyncTaskOnDatabase() {
        runningAsyncTasks--;
        if (waitingToClose) {
            close();
        }
    }

    public void startListening(SavedRecipesFragment savedRecipesFragment) {
        RecipesDataSource.savedRecipesFragment = savedRecipesFragment;
    }

    public void stopListening() {
        savedRecipesFragment = null;
    }

    private void notifyListeningActivity() {
        if (savedRecipesFragment != null) savedRecipesFragment.notifyRecipesListChanged();
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        if (runningAsyncTasks > 0) {
            waitingToClose = true;
        } else {
            closeDatabase();
        }
    }

    private void closeDatabase() {
        dbHelper.close();
    }

    public boolean isAlreadySaved(Recipe recipe) {
        RecipeDO recipeDO = getRecipeByUrl(recipe.getUrl());
        return recipeDO != null && recipeDO.equals(recipe);
    }

    public boolean isAlreadySaved(SavedRecipesPostModel recipe) {
        RecipeDO recipeDO = getRecipeByUrl(recipe.getUri());
        return recipeDO != null && recipeDO.equals(recipe);
    }

    private RecipeDO getRecipeByUrl(String url) {
        RecipeDO recipe = null;
        final Cursor cursor = database.query(
                SQLiteHelper.TABLE_RECIPES, allColumns, SQLiteHelper.COLUMN_URL + " LIKE \'%" + url + "%\'", null, null, null, null);
        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            recipe = cursorToRecipe(cursor);
        }
        cursor.close();
        return recipe;
    }

    private RecipeDO saveRecipeAndReturn(String label, String url, String photoUrl) {
        String photoPath = downloadPhotoAndSaveInLocalMemory(photoUrl, photoUrl.substring("https://www.edamam.com/web-img/".length()).replace('/', '-'));
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_LABEL, label);
        values.put(SQLiteHelper.COLUMN_URL, url);
        values.put(SQLiteHelper.COLUMN_CONTENT, getHtmlFromUrl(url));
        values.put(SQLiteHelper.COLUMN_PHOTO_PATH, photoPath);
        values.put(SQLiteHelper.COLUMN_PHOTO_URL, photoUrl);
        final long insertId = database.insert(SQLiteHelper.TABLE_RECIPES, null, values);
        final Cursor cursor
                = database.query(SQLiteHelper.TABLE_RECIPES, allColumns, SQLiteHelper.COLUMN_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        final RecipeDO recipeDO = cursorToRecipe(cursor);
        cursor.close();
        return recipeDO;
    }

    private RecipeDO saveRecipeAndReturn(SavedRecipesPostModel recipe) {
        return saveRecipeAndReturn(recipe.getName(), recipe.getUri(), recipe.getImageUrl());
    }

    private RecipeDO saveRecipeAndReturn(Recipe recipe) {
        return saveRecipeAndReturn(recipe.getLabel(), recipe.getUrl(), recipe.getImage());
    }

    public void saveRecipe(final SavedRecipesPostModel recipe) {
        if (isAlreadySaved(recipe)) {
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                startAsyncTaskOnDatabase();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                saveRecipeAndReturn(recipe);
                return null;
            }

            @Override
            protected void onPostExecute(Void object) {
                Log.d(RecipesDataSource.class.getSimpleName(), "Recipe saved");
                notifyListeningActivity();
                stopAsyncTaskOnDatabase();
            }
        }.execute();
    }

    public void saveRecipe(final Recipe recipe, final RecipeFragment.ButtonHolder buttonHolder) {
        if (isAlreadySaved(recipe)) {
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                startAsyncTaskOnDatabase();
                if (buttonHolder != null) {
                    buttonHolder.setBlocked(true);
                }
            }

            @Override
            protected Void doInBackground(Void... voids) {
                saveRecipeAndReturn(recipe);
                return null;
            }

            @Override
            protected void onPostExecute(Void object) {
                if (buttonHolder != null) {
                    buttonHolder.setBlocked(false);
                }
                Log.d(RecipesDataSource.class.getSimpleName(), "Recipe saved");
                notifyListeningActivity();
                stopAsyncTaskOnDatabase();
            }
        }.execute();
    }

    public void deleteRecipe(final RecipeDO recipe) {
        deleteRecipe(recipe, null);
    }

    public void deleteRecipe(final RecipeDO recipe, final RecipeFragment.ButtonHolder buttonHolder) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                startAsyncTaskOnDatabase();
                if (buttonHolder != null) {
                    buttonHolder.setBlocked(true);
                }
            }

            @Override
            protected Void doInBackground(Void... voids) {
                final long id = recipe.getId();
                deleteRecipePhotoFromLocalMemory(recipe.getPhotoPath());
                database.delete(SQLiteHelper.TABLE_RECIPES, SQLiteHelper.COLUMN_ID + " = " + id, null);
                return null;
            }

            @Override
            protected void onPostExecute(Void object) {
                if (buttonHolder != null) {
                    buttonHolder.setBlocked(false);
                }
                Log.d(RecipesDataSource.class.getSimpleName(), "Recipe deleted");
                notifyListeningActivity();
                stopAsyncTaskOnDatabase();
            }
        }.execute();
    }

    public void deleteRecipe(Recipe recipe, RecipeFragment.ButtonHolder buttonHolder) {
        final RecipeDO recipeDO = getRecipeByUrl(recipe.getUrl());
        if (recipeDO != null) {
            deleteRecipe(recipeDO, buttonHolder);
        }
        notifyListeningActivity();
    }

    public void deleteAll() {
        // Delete all photos stored in local memory.
        Cursor cursor = database.query(SQLiteHelper.TABLE_RECIPES, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            RecipeDO recipe = cursorToRecipe(cursor);
            deleteRecipePhotoFromLocalMemory(recipe.getPhotoPath());
            cursor.moveToNext();
        }
        cursor.close();

        // Delete entries in database.
        database.delete(SQLiteHelper.TABLE_RECIPES, null, null);
        Log.d(getClass().getSimpleName(), "All recipeList deleted");
        notifyListeningActivity();
    }

    public List<RecipeDO> getAllRecipes() {
        final Cursor cursor = database.query(SQLiteHelper.TABLE_RECIPES, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        List<RecipeDO> recipes = new ArrayList<>();

        while (!cursor.isAfterLast()) {
            RecipeDO recipe = cursorToRecipe(cursor);
            if (recipe != null) {
                recipes.add(recipe);
            }
            cursor.moveToNext();
        }
        cursor.close();
        return recipes;
    }

    private RecipeDO cursorToRecipe(Cursor cursor) {
        return new RecipeDO(
                cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));
    }

    private static String getHtmlFromUrl(String source) {
        InputStream is = null;
        String content = "";
        try {
            final URL url = new URL(source);
            is = url.openStream();
            final BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                content += line;
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        Log.d(RecipesDataSource.class.getSimpleName(), "content: " + content);
        return content;
    }

    private void deleteRecipePhotoFromLocalMemory(final String photoPath) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                startAsyncTaskOnDatabase();
                FilesManager.deletePhotoFromMemory(context, photoPath);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                stopAsyncTaskOnDatabase();
            }
        }.execute();
    }

    private String downloadPhotoAndSaveInLocalMemory(final String imageUrl, final String filename) {
        Log.d(getClass().getSimpleName(), "Photo download started");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                startAsyncTaskOnDatabase();
                try {
                    FilesManager.downloadPhotoAndGetFile(imageUrl, filename);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(RecipesDataSource.class.getSimpleName(), "Couldn't save photo file for recipe");
                }
                Log.d(RecipesDataSource.class.getSimpleName(), "Saved photo " + filename);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                stopAsyncTaskOnDatabase();
            }
        }.execute();

        return getFilesDirectoryPath() + filename;
    }
}