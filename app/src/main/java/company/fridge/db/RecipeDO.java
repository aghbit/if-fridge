package company.fridge.db;

import android.os.Parcel;
import android.os.Parcelable;

import company.fridge.model.Recipe;
import company.fridge.model.SavedRecipesPostModel;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = {"id", "content", "photoPath"})
public class RecipeDO implements Parcelable {

    private final long id;
    private final String label;
    private final String url;
    private final String content;
    private final String photoPath;
    private final String photoUrl;

    public RecipeDO(long id, String label, String url, String content, String photoPath, String photoUrl) {
        this.id = id;
        this.label = label;
        this.url = url;
        this.content = content;
        this.photoPath = photoPath;
        this.photoUrl = photoUrl;
    }

    // Parcelable part
    protected RecipeDO(Parcel in) {
        id = in.readLong();
        label = in.readString();
        url = in.readString();
        content = in.readString();
        photoPath = in.readString();
        photoUrl = in.readString();
    }

    public static final Creator<RecipeDO> CREATOR = new Creator<RecipeDO>() {
        @Override
        public RecipeDO createFromParcel(Parcel in) {
            return new RecipeDO(in);
        }

        @Override
        public RecipeDO[] newArray(int size) {
            return new RecipeDO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(label);
        dest.writeString(url);
        dest.writeString(content);
        dest.writeString(photoPath);
        dest.writeString(photoUrl);
    }

    public boolean equals(Recipe recipe) {
        return this.getUrl().equals(recipe.getUrl()) && this.getLabel().equals(recipe.getLabel());
    }

    public boolean equals(SavedRecipesPostModel recipe) {
        return this.getUrl().equals(recipe.getUri()) && this.getLabel().equals(recipe.getName());
    }
}