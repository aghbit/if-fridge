package company.fridge.request;

import java.util.List;

import company.fridge.model.RecipeUrlData;
import lombok.Data;

@Data
public class UrlCreator {

    private static final String REQUEST_GET_RECIPES_PART = "/recipes?";
    private static final String REQUEST_GET_LABELS_PART = "/labels";
    private static final String REQUEST_USER_LABELS_PART = "/app/user/prefs";
    private static final String API_URL = "https://if-fridge.herokuapp.com";

    private static String ingredientsPart = "q=";
    private static String rangeFromPart = "from=";
    private static String rangeToPart = "to=";
    private static String caloriesInfoPart = "calories=gte{0},lte{1}";
    private static String healthInfoPart = "health=";
    private static String dietInfoPart = "diet=";

    private StringBuilder appendListToStringBuilder(StringBuilder stringBuilder, List<String> list) {
        for (String item : list) {
            if (item.contains(" ")) stringBuilder.append(item.replace(" ", "%20"));
            else stringBuilder.append(item);
            stringBuilder.append(",");
        }
        // remove unnecessary comma after last list item
        stringBuilder = new StringBuilder(stringBuilder.substring(0, stringBuilder.length() - 1));
        return stringBuilder;
    }

    public String createForRecipes(RecipeUrlData data) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(API_URL);
        stringBuilder.append(REQUEST_GET_RECIPES_PART);
        stringBuilder.append(ingredientsPart);
        if (data.getIngredients() != null) {
            stringBuilder = this.appendListToStringBuilder(stringBuilder, data.getIngredients());
        }
        stringBuilder.append("&");
        stringBuilder.append(rangeFromPart);
        stringBuilder.append(data.getFrom());
        stringBuilder.append("&");
        stringBuilder.append(rangeToPart);
        stringBuilder.append(data.getTo());
        if (data.getCaloriesFrom() != data.getCaloriesTo()) {
            stringBuilder.append("&");
            caloriesInfoPart = caloriesInfoPart.replace("{0}", String.valueOf(data.getCaloriesFrom()));
            caloriesInfoPart = caloriesInfoPart.replace("{1}", String.valueOf(data.getCaloriesTo()));
            stringBuilder.append(caloriesInfoPart);
        }
        if (data.getHealthLabels() != null && !data.getHealthLabels().isEmpty()) {
            stringBuilder.append("&");
            stringBuilder.append(healthInfoPart);
            stringBuilder.append(data.getHealthLabels().get(0));
        }

        if (data.getDietLabels() != null && !data.getDietLabels().isEmpty()) {
            stringBuilder.append("&");
            stringBuilder.append(dietInfoPart);
            stringBuilder.append(data.getDietLabels().get(0));
        }
        return stringBuilder.toString();
    }

    public String createForLabels() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(API_URL);
        stringBuilder.append(REQUEST_GET_LABELS_PART);

        return stringBuilder.toString();
    }
}
