package company.fridge.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class UserPreferencesPostModel {
    List<String> healthPrefs;
    List<String> dietPrefs;
}
