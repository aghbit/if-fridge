package company.fridge.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class Recipe implements Parcelable {

    @SerializedName("uri")
    private String uri;
    @SerializedName("label")
    private String label;
    @SerializedName("imageUrl")
    private String image;
    @SerializedName("source")
    private String source;
    @SerializedName("recipeUrl")
    private String url;

    private List<String> dietLabels;
    private List<String> healthLabels;
    private List<String> ingredients;
    private int yield;
    private double calories;
    private double caloriesForPortion;
    private int numOfDownloads;
    private double fat;                     // in grams
    private double carbs;                   // in grams

    private Recipe(Parcel in) {
        uri = in.readString();
        label = in.readString();
        image = in.readString();
        source = in.readString();
        url = in.readString();
        dietLabels = in.createStringArrayList();
        healthLabels = in.createStringArrayList();
        ingredients = in.createStringArrayList();
        yield = in.readInt();
        calories = in.readDouble();
        caloriesForPortion = in.readDouble();
        numOfDownloads = in.readInt();
        fat = in.readDouble();
        carbs = in.readDouble();
    }

    public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
        @Override
        public Recipe createFromParcel(Parcel in) {
            return new Recipe(in);
        }

        @Override
        public Recipe[] newArray(int size) {
            return new Recipe[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uri);
        dest.writeString(label);
        dest.writeString(image);
        dest.writeString(source);
        dest.writeString(url);
        dest.writeStringList(dietLabels);
        dest.writeStringList(healthLabels);
        dest.writeStringList(ingredients);
        dest.writeInt(yield);
        dest.writeDouble(calories);
        dest.writeDouble(caloriesForPortion);
        dest.writeInt(numOfDownloads);
        dest.writeDouble(fat);
        dest.writeDouble(carbs);
    }
}
