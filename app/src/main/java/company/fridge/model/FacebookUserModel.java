package company.fridge.model;

import lombok.Data;


@Data
public class FacebookUserModel {
    private String name;
    private String id;
}
