package company.fridge.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class SavedRecipesPostModel {
    private String uri;
    private String name;
    private String imageUrl;

    public SavedRecipesPostModel(Recipe recipe) {
        this.uri = recipe.getUrl();
        this.name = recipe.getLabel();
        this.imageUrl = recipe.getImage();
    }
}
