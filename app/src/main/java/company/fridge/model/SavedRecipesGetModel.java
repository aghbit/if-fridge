package company.fridge.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class SavedRecipesGetModel {
    private List<SavedRecipesPostModel> recipes;
    private long timestamp;
}
