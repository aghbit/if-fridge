package company.fridge.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class UserPreferencesGetModel {
    private List<String> healthPrefs;
    private List<String> dietPrefs;
    private long timestamp;
}
