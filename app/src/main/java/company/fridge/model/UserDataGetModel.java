package company.fridge.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class UserDataGetModel {
    private String username;
    private String imageUrl;
}
