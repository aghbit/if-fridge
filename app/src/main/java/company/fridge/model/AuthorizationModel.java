package company.fridge.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class AuthorizationModel {
    private String id;
    private String token;
    private String facebookId;
}
