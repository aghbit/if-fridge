package company.fridge.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class RecipeUrlData {
    private final List<String> ingredients;
    private final int from;
    private final int to;
    private final int caloriesFrom;
    private final int caloriesTo;
    private final List<String> healthLabels;
    private final List<String> dietLabels;
}
