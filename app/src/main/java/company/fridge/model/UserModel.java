package company.fridge.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class UserModel {
    private String email;
    private String password;
}
