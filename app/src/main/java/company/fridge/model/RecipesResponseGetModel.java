package company.fridge.model;

import java.util.List;

import lombok.Data;

@Data
public class RecipesResponseGetModel {
    private List<Recipe> recipes;
}
