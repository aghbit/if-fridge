package company.fridge.model;

import java.util.List;

import lombok.Data;

@Data
public class LabelGetModel {
    List<String> healthLabels;
    List<String> dietLabels;
}
