package company.fridge.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import lombok.Data;

@Data
public class Ingredient implements Parcelable, Comparable {

    private final String name;
    private Boolean checkbox;

    public Ingredient(String name) {
        this.name = name;
        this.checkbox = false;
    }

    protected Ingredient(Parcel in) {
        name = in.readString();
        checkbox = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Creator<Ingredient> CREATOR = new Creator<Ingredient>() {
        @Override
        public Ingredient createFromParcel(Parcel in) {
            return new Ingredient(in);
        }

        @Override
        public Ingredient[] newArray(int size) {
            return new Ingredient[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeValue(checkbox);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        if (o.getClass() == Ingredient.class) {
            Ingredient ingredient = (Ingredient) o;
            return this.getName().compareTo(ingredient.getName());
        }
        return 0;
    }
}
