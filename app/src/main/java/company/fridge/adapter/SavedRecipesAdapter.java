package company.fridge.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import company.fridge.activity.AuthorizedAppCompatActivity;
import company.fridge.activity.RecipeWebViewActivity;
import company.fridge.db.RecipeDO;
import company.fridge.db.RecipesDataSource;
import company.fridge.manager.SavedRecipesManager;
import company.fridge.R;
import lombok.Data;

@Data
public class SavedRecipesAdapter extends ArrayAdapter<RecipeDO> {

    private Fragment fragment;
    private List<RecipeDO> recipeList;
    private RecipesDataSource dataSource;

    public SavedRecipesAdapter(Fragment fragment, RecipesDataSource dataSource, int textViewResourceId, List<RecipeDO> recipeList) {
        super(fragment.getContext(), textViewResourceId, recipeList);
        this.fragment = fragment;
        this.recipeList = recipeList;
        this.dataSource = dataSource;
    }

    private class ViewHolder {
        TextView label;
        ImageView image;
        ImageButton button;
    }

    @Override
    public int getCount() {
        return recipeList.size();
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.saved_recipe, null);
            holder = new ViewHolder();
            holder.label = (TextView) convertView.findViewById(R.id.saved_recipe_label);
            holder.image = (ImageView) convertView.findViewById(R.id.saved_recipe_image);
            holder.button = (ImageButton) convertView.findViewById(R.id.x_button);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (position >= getCount()) {
            return convertView;
        }
        final RecipeDO recipe = recipeList.get(position);

        holder.label.setText(recipe.getLabel());
        holder.image.setImageBitmap(BitmapFactory.decodeFile(recipe.getPhotoPath()));

        holder.label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRecipeDetails(recipe);
            }
        });
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRecipeDetails(recipe);
            }
        });
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SavedRecipesManager().deleteRecipe(dataSource, recipe, (AuthorizedAppCompatActivity) fragment.getActivity());
                Toast.makeText(getContext(), "Recipe " + recipe.getLabel() + " deleted", Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;
    }

    private void openRecipeDetails(RecipeDO recipe) {
        Intent intent = new Intent(fragment.getActivity().getApplicationContext(), RecipeWebViewActivity.class);
        intent.putExtra("recipe", recipe);
        fragment.getActivity().startActivity(intent);
    }
}
