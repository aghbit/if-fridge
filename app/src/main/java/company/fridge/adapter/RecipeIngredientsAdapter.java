package company.fridge.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import company.fridge.R;


public class RecipeIngredientsAdapter extends ArrayAdapter<String> {

    public RecipeIngredientsAdapter(Context context, int resource, List<String> items) {
        super(context, resource, items);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.recipe_ingredient_cell, null);
        }
        final String ingredient = getItem(position);
        if (ingredient != null) {
            TextView textView = (TextView) view.findViewById(R.id.ingredient_text_view);
            if (textView != null) textView.setText(ingredient);
        }
        return view;
    }
}
