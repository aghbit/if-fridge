package company.fridge.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import company.fridge.model.Ingredient;
import company.fridge.R;

public class IngredientsAdapter extends BaseAdapter implements Filterable {

    private ArrayList<Ingredient> ingredients;
    private final ArrayList<Ingredient> copyOfIngredients;
    private final Context context;

    public IngredientsAdapter(Context context, ArrayList<Ingredient> ingredients) {
        this.context = context;
        Collections.sort(ingredients);
        this.ingredients = ingredients;
        this.copyOfIngredients = new ArrayList<>(ingredients);
    }

    @Override
    public int getCount() {
        return ingredients.size();
    }

    @Override
    public Object getItem(int position) {
        return ingredients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        Ingredient ingredient = (Ingredient) getItem(position);
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.ingredient_checkbox, parent, false);
        }

        final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkbox1);
        checkBox.setText(ingredient.getName());
        checkBox.setChecked(ingredient.getCheckbox());
        checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                final CheckBox checkBox = (CheckBox) view;
                ingredients.get(position).setCheckbox(checkBox.isChecked());
            }
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                ingredients = (ArrayList<Ingredient>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<Ingredient> filteredIngredients = new ArrayList<>();

                if (constraint == null || constraint.length() == 0) {
                    results.count = copyOfIngredients.size();
                    results.values = copyOfIngredients;
                } else {
                    for (Ingredient ingredient : copyOfIngredients) {
                        if (ingredient.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                            filteredIngredients.add(ingredient);
                        }
                    }
                    results.count = filteredIngredients.size();
                    results.values = filteredIngredients;
                }
                return results;
            }
        };
    }
}