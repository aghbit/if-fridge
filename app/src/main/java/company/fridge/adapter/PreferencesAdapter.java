package company.fridge.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import company.fridge.model.Preference;
import company.fridge.fragment.UserProfileFragment;
import company.fridge.R;
import lombok.AllArgsConstructor;
import lombok.Data;

public class PreferencesAdapter extends ArrayAdapter<Preference> {

    private static final int TYPE_PREFERENCE = 0;
    private static final int TYPE_HEALTH_LABELS_HEADER = 1;
    private static final int TYPE_DIET_LABELS_HEADER = 2;

    private List<Preference> healthPreferences;
    private List<Preference> dietPreferences;
    private UserProfileFragment fragment;

    public PreferencesAdapter(UserProfileFragment fragment, int textViewResourceId, List<Preference> healthPreferences,
                              List<Preference> dietPreferences, List<Preference> allPreferences) {
        super(fragment.getContext(), textViewResourceId, allPreferences);
        this.healthPreferences = healthPreferences;
        this.dietPreferences = dietPreferences;
        this.fragment = fragment;
    }

    @Override
    public int getViewTypeCount() {
        return 5;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return TYPE_HEALTH_LABELS_HEADER;
        else if (position == healthPreferences.size() + 1) return TYPE_DIET_LABELS_HEADER;
        else return TYPE_PREFERENCE;
    }

    @Nullable
    @Override
    public Preference getItem(int position) {
        if (position > 0 && position < healthPreferences.size() + 1) {
            return healthPreferences.get(position - 1);
        } else if (position > healthPreferences.size() + 1) {
            return dietPreferences.get(position - (healthPreferences.size() + 2));
        } else return null;
    }

    private List<Preference> getPreferencesOfType(int position) {
        if (position > 0 && position < healthPreferences.size() + 1) {
            return healthPreferences;
        } else if (position > healthPreferences.size() + 1) {
            return dietPreferences;
        } else return null;
    }

    @Override
    public int getCount() {
        return super.getCount() + 2;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        int listViewItemType = getItemViewType(position);

        if (listViewItemType == TYPE_PREFERENCE) {
            Preference preference = getItem(position);
            PreferenceHolder preferenceHolder;

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.preference_radio_button, parent, false);

                TextView name = (TextView) convertView.findViewById(R.id.preference_name);
                RadioButton preferenceRadioButton = (RadioButton) convertView.findViewById(R.id.radioButton1);

                preferenceHolder = new PreferenceHolder(name, preferenceRadioButton);

                convertView.setTag(preferenceHolder);

            } else preferenceHolder = (PreferenceHolder) convertView.getTag();

            preferenceHolder.getPreferenceNameTextView().setText(preference.getName());
            preferenceHolder.getPreferenceRadioButton().setChecked(preference.getValue());

            preferenceHolder.getPreferenceRadioButton().setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    final RadioButton radioButton = (RadioButton) view;
                    Preference preference = getItem(position);
                    if (preference != null) {
                        boolean alreadyChecked = preference.getValue();
                        if (!alreadyChecked) {
                            preference.setValue(true);
                            changeUserPreferences(position, preference);
                        } else {
                            preference.setValue(false);
                            radioButton.setChecked(false);
                        }
                        fragment.saveUserPreferences();
                    }
                }
            });

        } else if (listViewItemType == TYPE_HEALTH_LABELS_HEADER || listViewItemType == TYPE_DIET_LABELS_HEADER) {
            HeaderHolder headerHolder;

            if (convertView == null) {
                final LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.preference_header, parent, false);

                final TextView textView = (TextView) convertView.findViewById(R.id.preference_header);
                headerHolder = new HeaderHolder(textView);
                convertView.setTag(headerHolder);

            } else headerHolder = (HeaderHolder) convertView.getTag();

            String textToSet;
            if (listViewItemType == TYPE_HEALTH_LABELS_HEADER) textToSet = fragment.getActivity().getString(R.string.health_labels);
            else textToSet = fragment.getActivity().getString(R.string.diet_labels);

            headerHolder.getTextView().setText(textToSet);
        }

        return convertView;
    }

    private void changeUserPreferences(int position, Preference checkedPreference) {
        List<Preference> preferences = getPreferencesOfType(position);
        for (Preference preference : preferences) {
            if (preference.getName().equals(checkedPreference.getName())) {
                preference.setValue(true);
            } else {
                preference.setValue(false);
            }
        }
        notifyDataSetChanged();
    }

    public void refreshUserPreferences(List<Preference> healthPreferences, List<Preference> dietPreferences) {
        this.healthPreferences = healthPreferences;
        this.dietPreferences = dietPreferences;
        notifyDataSetChanged();
    }

    @Data
    @AllArgsConstructor(suppressConstructorProperties = true)
    private class HeaderHolder {
        private TextView textView;
    }

    @Data
    @AllArgsConstructor(suppressConstructorProperties = true)
    private class PreferenceHolder {
        private TextView preferenceNameTextView;
        private RadioButton preferenceRadioButton;
    }
}